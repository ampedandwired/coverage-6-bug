# Coverage 6 Bug

Test case for coverage 6 bug. Steps to reproduce:

```
$ pip install tox
$ tox -e cov6
```

This will fail with `CoverageWarning: No data was collected`.

You can run the same test using coverage 5.5 with `tox -e cov5`.
This succeeds and reports 100% test coverage.

If you run coverage 6.0 without tox it works as expected:
```
$ pip install coverage==6.0
$ PYTHONPATH=src coverage run --source=foo test_add.py && coverage report
```

And if you run without using namespace packages it also works. To do this
change `find_namepace:` in `setup.py` to just `find:` and run the following commands:

```
$ touch src/foo/__init__.py
$ tox -e cov6
```

So it seems that coverage 6.0, tox and namespace packages are all necessary conditions to reproduce this bug.
